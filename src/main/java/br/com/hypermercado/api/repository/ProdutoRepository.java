package br.com.hypermercado.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hypermercado.api.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long>{

}
