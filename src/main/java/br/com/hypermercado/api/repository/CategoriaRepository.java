package br.com.hypermercado.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hypermercado.api.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long>{

}
