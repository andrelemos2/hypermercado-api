package br.com.hypermercado.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import br.com.hypermercado.api.config.property.HypermercadoApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(HypermercadoApiProperty.class)
public class HypermercadoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HypermercadoApiApplication.class, args);
	}
}
