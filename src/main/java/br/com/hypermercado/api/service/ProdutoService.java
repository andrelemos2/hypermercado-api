package br.com.hypermercado.api.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.hypermercado.api.model.Produto;
import br.com.hypermercado.api.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository repository;
	
	public Produto criar(Produto produto) {
		 return repository.save(produto);
	}

	public Produto atualizar(Long codigo, Produto produto) {
		Produto produtoSalvo = buscarProdutoPorCodigo(codigo);
		BeanUtils.copyProperties(produto, produtoSalvo, "codigo");
		return repository.save(produtoSalvo);
	}

	public void atualizarPropriedadeAtivo(Long codigo, Boolean ativo) {
		Produto produtoSalvo = buscarProdutoPorCodigo(codigo);
		produtoSalvo.setAtivo(ativo);
		repository.save(produtoSalvo);
	}
	
	public List<Produto> listar() {
		return repository.findAll();
	}
	
	public Produto buscarProdutoPorCodigo(Long codigo) {
		Produto produtoSalva = repository.findOne(codigo);
		if (produtoSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return produtoSalva;
	}

	public void remover(Long codigo) {
		repository.delete(codigo);
	}
}
