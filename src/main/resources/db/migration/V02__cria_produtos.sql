CREATE TABLE produto (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(60) NOT NULL,
    codigo_categoria BIGINT(20) NOT NULL,
    valor NUMERIC(10,2) NOT NULL DEFAULT 0,
    ativo BOOLEAN NOT NULL DEFAULT TRUE,
    FOREIGN KEY (codigo_categoria) REFERENCES categoria(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO produto (nome, codigo_categoria, valor, ativo) values ('Playstation 4', 1, 1500.00, true);
INSERT INTO produto (nome, codigo_categoria, valor, ativo) values ('Samsumg Smart TV 4K', 1, 2399.00, true);
INSERT INTO produto (nome, codigo_categoria, valor, ativo) values ('Google Chrome Cast 2G', 1, 200.00, true);
INSERT INTO produto (nome, codigo_categoria, valor, ativo) values ('PES 2018', 1, 250.00, true);
INSERT INTO produto (nome, codigo_categoria, valor, ativo) values ('Aprendendo Java 9', 4, 50.00, true);